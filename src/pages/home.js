import { Grid } from '@material-ui/core'
import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Post from '../components/post/Post'
import Profile from '../components/profile/Profile';
import PostSkeleton from '../utils/PostSkeleton';

import { connect } from 'react-redux';
import { getPosts } from '../redux/actions/dataActions';
 
 class home extends Component {
    componentDidMount(){
       /*  axios
            .get('/posts')
            .then((res) =>{
                console.log(res);
                this.setState({
                    posts: res.data       //in axios sta nell'attributo data il valore
                });
            })
            .catch(err => console.log(err)); */
        this.props.getPosts();
    }
    render() {
        const {posts, loading} = this.props.data;
        let recentPostsMarkup = !loading ? (                   //se ci sono post li visualizza uno ad uno
        posts.map(post => 
        <Post key={post.postId} post={post}/>)
        ) : (
            <PostSkeleton/>
            );
        return (
           <Grid container>
               <Grid item sm={4} xs={12}>
                <Profile/>
               </Grid>
               <Grid item sm={8} xs={12}>
                {recentPostsMarkup}
               </Grid>
               <Grid item sm={4} xs={12}>
                
               </Grid>
           </Grid>
        )
    }
}

home.propTypes={
    getPosts: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired
}

const mapStateToProps = state =>({
    data: state.data
})

export default connect(mapStateToProps,{getPosts})(home)
