import React, { Component } from 'react'
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';
import AppIcon from '../images/WP_logo.png';
import {Link} from 'react-router-dom';
//MUI
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Buttom from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

import { connect } from 'react-redux';
import { signupUser } from '../redux/actions/userAction'

const styles = (theme) => ({
    ...theme.spreadThis,
    typography:{
        useNextVariants: true
      },
      form: {
        textAlign: 'center'
    },
      image:{
        margin: '20px auto 20px auto',
        width: '30%'
      },
      pageTitle:{
        margin:'10px auto 10px auto',
      },
      textField:{
        margin:'10px auto 10px auto',
      },
      buttom:{
        marginTop: '20px',
        position:'relative'
      },
    customError:{
        color: 'red',
        fontSize: '0,8rem',
        marginTop: 10,
    },
    progress:{
        position:'absolute'
    }
})


class signup extends Component {

    constructor(){
        super();
        this.state={
            email:'',
            password: '',
            confirmPassword:'',
            handle:'',
            errors:{},
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.UI.errors) {
          this.setState({ errors: nextProps.UI.errors });
        }
    }

    handleSubmit = (event) =>{
        event.preventDefault();
        this.setState({
            loading: true
        });
        const newUserData={
            email: this.state.email,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword,
            handle: this.state.handle
        }
        this.props.signupUser(newUserData, this.props.history);
    };

    handleChange= (event) =>{
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    render() {
        const { classes,UI: { loading } } = this.props;
        const { errors } =this.state;
        return (
          <Grid container className={classes.form}>
              <Grid item sm/>
              <Grid item sm>
                <img src={AppIcon} alt={'logo'} className={classes.image}/>
                <Typography variant="h2" className={classes.pageTitle}>
                    Signup
                </Typography>
                <form noValidate onSubmit={this.handleSubmit}>
                    <TextField 
                        id="email" 
                        name="email" 
                        type="email" 
                        label="Email" 
                        className={classes.textField}
                        helperText={errors.email}
                        error={errors.email ? true : false}
                        value={this.state.email}
                        onChange={this.handleChange}
                        fullWidth
                    />
                    <TextField 
                        id="password" 
                        name="password"
                        type="password" 
                        label="Password" 
                        className={classes.textField}
                        value={this.state.password}
                        helperText={errors.password}
                        error={errors.password ? true : false}
                        onChange={this.handleChange}
                        fullWidth
                    />
                    <TextField 
                        id="confirmPassword" 
                        name="confirmPassword"
                        type="password" 
                        label="Conferma Password" 
                        className={classes.textField}
                        value={this.state.confirmPassword}
                        helperText={errors.confirmPassword}
                        error={errors.confirmPassword ? true : false}
                        onChange={this.handleChange}
                        fullWidth
                    />
                    <TextField 
                        id="handle" 
                        name="handle"
                        type="text" 
                        label="Username" 
                        className={classes.textField}
                        value={this.state.handle}
                        helperText={errors.handle}
                        error={errors.handle ? true : false}
                        onChange={this.handleChange}
                        fullWidth
                    />
                    {errors.general && (
                        <Typography variant="body2" className={classes.customError}>
                            {errors.general}
                        </Typography>
                    )}
                    <Buttom type="submit" variant="contained" color="primary" className={classes.button} disabled={loading}>
                        Registrati
                        {loading && (
                            <CircularProgress size={500} className={classes.progress}/>
                        ) }
                    </Buttom>
                    <br/>
                    <small> Hai già un account? Accedi <Link to="/login"> Qui </Link></small>
                </form>
              </Grid>
              <Grid item sm/>
          </Grid>
        )
    }
}

signup.propTypes={
    classes: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    UI: PropTypes.object.isRequired,
    signupUser: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
    user: state.user,
    UI: state.UI
});

export default connect(mapStateToProps,{signupUser})(withStyles(styles)(signup));
