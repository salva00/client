import {SET_USER, SET_AUTHENTICATED,SET_UNAUTHENTICATED,LOADING_USER,GG_POST, UNGG_POST,MARK_NOTIFICATIONS_READ} from '../types';

const initialState = {
    authenticated: false,
    loading:false,
    credentials:{},
    GGs: [],
    notifications:[],
};
// eslint-disable-next-line
export default function(state = initialState, action){
    switch(action.type){
        case SET_AUTHENTICATED:
            return {
                ...state,
                authenticated: true
            };
        case SET_UNAUTHENTICATED:
            return initialState;
        case SET_USER:
            return {
                authenticated: true,
                loading: false,
                ...action.payload
            };
        case LOADING_USER:
            return {
            ...state,
            loading: true
            };
        case GG_POST:
            return{
                ...state,
                GGs:[
                    ...state.GGs,
                    {
                        userHandle:state.credentials.handle,
                        postId:action.payload.postId
                    }
                ]
            }
        case UNGG_POST:
            return{
                ...state,
                GGs: state.GGs.filter(gg=>gg.postId!==action.payload.postId)
            }
        case MARK_NOTIFICATIONS_READ:
            state.notifications.forEach((not) => (not.read = true));
            return {
                ...state
            };
        default:
            return state;
    }
}