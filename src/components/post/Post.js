import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';

import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { ggPost,unggPost } from '../../redux/actions/dataActions';

import DeletePost from './DeletePost'
import PostDialog from './PostDialog'
import GgButton from './GgButton'
//MUI
import withStyles from '@material-ui/core/styles/withStyles'
import CardMedia from '@material-ui/core/CardMedia';
//import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
//import CardActions from '@material-ui/core/CardActions';
//import CardActionArea from '@material-ui/core/CardActionArea';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import ChatIcon from '@material-ui/icons/Chat'
import MyButton from '../../utils/MyButton';

const styles = {
    card: {
        display: "flex",
        marginBottom:20,

    },
    image:{
        minWidth: 100,
        minHeight: 100,
        objectFit: 'cover'
    },
    content:{
        objectFit: 'cover'
    },
    
    

}

class Post extends Component {
    ggedPost = () =>{
        if(this.props.user.GGs && this.props.user.GGs.find(gg => gg.postId === this.props.post.postId)){
            return true;
        }
        else{
            return false;
        }
    }
    ggPost = () =>{
        this.props.ggPost(this.props.post.postId);
    }
    unggPost = () =>{
        this.props.unggPost(this.props.post.postId);
    }
    
    render() {
        
        //const {classes} = this.props; => destructuring ovvero const classes = this.props.classes
        // eslint-disable-next-line
        dayjs.extend(relativeTime);
        const {
            classes,
            post: {
                body, createdAt, userImage,userHandle,postId,ggCount,commentCount
            },
            user:{
                authenticated, credentials:{
                    handle
                }
            }} = this.props;
        /* const ggButton = !authenticated ? (
            <MyButton tip="GG">
                <Link to="/login">
                    <FavoriteBorder color="primary"/>
                </Link>
            </MyButton>
        ): (
            this.ggedPost() ? (
                <MyButton tip ="rimuovi GG" onClick={this.unggPost}>
                    <FavoriteIcon color="primary"/>
                </MyButton>
            ):(
                <MyButton tip ="GG" onClick={this.ggPost}>
                    <FavoriteBorder color="primary"/>
                </MyButton>
            )
        ); */

        const deleteButton = authenticated && userHandle === handle ? (
            <DeletePost postId={postId}/>
        ): null;

        return (
            <Card className={classes.card}>
                <CardMedia
                    image={userImage}
                    title="Profile image"
                    className={classes.image}
                    />
                    <CardContent className={classes.content}>
                        <Typography 
                            variant="h5"
                            component={Link} 
                            to={`/users/${userHandle}`} 
                            color="primary">
                            {userHandle}
                        </Typography>
                        {deleteButton}
                        <Typography variant="body2" color="textSecondary">
                            {dayjs(createdAt).fromNow()}
                        </Typography>
                        <Typography variant="body1">{body}</Typography>
                        <GgButton postId={postId}/>
                        <span>{ggCount} GG</span>
                        <MyButton tip="comments">
                            <ChatIcon color="primary"/>
                        </MyButton>
                        <span>{commentCount} commenti</span>
                        <PostDialog postId={postId} userHandle={userHandle} openDialog={this.props.openDialog}/>
                    </CardContent>
            </Card>
        )
    }
}

Post.propTypes = {
    ggPost: PropTypes.func.isRequired,
    unggPost: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    post: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    openDialog:PropTypes.bool
}

const mapStateToProps = state =>({
    user: state.user
})

const mapActionToProps = {
    ggPost,
    unggPost
}

export default connect(mapStateToProps,mapActionToProps)(withStyles(styles)(Post));
