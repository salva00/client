import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import MyButton from '../../utils/MyButton'
//REDUX
import  {connect} from 'react-redux';
import { addPost, clearErrors } from '../../redux/actions/dataActions';

//MUI
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import AddIcon from '@material-ui/icons/Add'
import CloseIcon from '@material-ui/icons/Close'

import CircularProgress from '@material-ui/core/CircularProgress'

const styles = {
    typography: {
        useNextVariants: true
      },
      form: {
        textAlign: 'center'
      },
      image: {
        margin: '20px auto 20px auto'
      },
      pageTitle: {
        margin: '10px auto 10px auto'
      },
      textField: {
        margin: '10px auto 10px auto'
      },
      button: {
        marginTop: 20,
        position: 'relative'
      },
      customError: {
        color: 'red',
        fontSize: '0.8rem',
        marginTop: 10
      },
    submitButton:{
        position:'relative'
    },
    progressSpinner:{
        position:'absolute'
    },
    closeButton:{
        position: 'absolute',
        left:'90%',
        top:'10%'
    }
};

class NewPost extends Component {

    state = {
        open:false,
        body:'',
        errors:{}
    };

    /* UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.UI.errors) {
            console.log('sono qui');
          this.setState({
            errors: nextProps.UI.errors
          });
          console.log(this.state.errors)
        }
        if (!nextProps.UI.errors && !nextProps.UI.loading) {
          this.setState({ body: '', open: false, errors: {} });
        }
    }; */

    //TODO: aggiustare questa

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.UI.errors) {
          this.setState({
            errors: nextProps.UI.errors
          });
          console.log(nextProps.UI.errors);
        }
        if (!nextProps.UI.errors && !nextProps.UI.loading) {
          this.setState({ body: '', open: false, errors: {} });
        }
      }


    handleOpen = () => {
        this.setState({open: true})
    };
    handleClose = () => {
        this.props.clearErrors();
        this.setState({open: false, errors:{}})
    };
    handleSubmit=(event)=>{
        event.preventDefault();
        //try {
            this.props.addPost({body: this.state.body});
        
       /*  }catch (err) {
        
        } */

    };
    handleChange=(event)=>{
        this.setState({[event.target.name]: event.target.value})
    };

    render() {
        const {errors} = this.state;
        const {
            classes,
            UI: { loading }
          } = this.props;
        return (
            <Fragment>
                <MyButton onClick={this.handleOpen} tip="Posta qualcosa!">
                    <AddIcon />
                </MyButton>
                <Dialog open={this.state.open} onClose={this.handleClose} fullWidth maxWidth='sm'>
                    <MyButton tip="Chiudi" onClick={this.handleClose} tipClassName={classes.closeButton}>
                        <CloseIcon/>
                    </MyButton>
                    <DialogTitle>Aggiungi un nuovo Post!</DialogTitle>
                    <DialogContent>
                        <form onSubmit={this.handleSubmit}>
                            <TextField
                                name="body"
                                type="text"
                                label="Nuovo Post"
                                placeholder="Tifone nega, CHANGE MY MIND!"
                                multiline 
                                rows="10"
                                error={errors.body ? true : false}
                                helperText={errors.body}
                                className={classes.TextField}
                                onChange={this.handleChange}
                                fullWidth
                            />
                            <Button 
                                type="submit" 
                                variant="contained" 
                                color="primary" 
                                className={classes.submitButton} 
                                disabled={loading}
                            >
                                Pubblica
                                {loading && (
                                    <CircularProgress size={50} className={classes.progressSpinner}/>)
                                }
                                
                            </Button>
                        </form>
                    </DialogContent>
                </Dialog>
            </Fragment>
        )
    }
}

NewPost.propTypes = {
    addPost: PropTypes.func.isRequired,
    clearErrors:PropTypes.func.isRequired,
    UI: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    UI: state.UI
  });

export default connect(mapStateToProps,{addPost,clearErrors})(withStyles(styles)(NewPost))
