import React, { Component } from 'react';
import MyButton from '../../utils/MyButton';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
// Icons
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
// REdux
import { connect } from 'react-redux';
import { ggPost, unggPost } from '../../redux/actions/dataActions';

export class GgButton extends Component {
    ggedPost = () =>{
        if(this.props.user.GGs && this.props.user.GGs.find(gg => gg.postId === this.props.postId)){
            return true;
        }
        else{
            return false;
        }
    }
  ggPost = () => {
    this.props.ggPost(this.props.postId);
  };
  unggPost = () => {
    this.props.unggPost(this.props.postId);
  };
  render() {
    const { authenticated } = this.props.user;
    const ggButton = !authenticated ? (
      <Link to="/login">
        <MyButton tip="gg">
          <FavoriteBorder color="primary" />
        </MyButton>
      </Link>
    ) : this.ggedPost() ? (
      <MyButton tip="Undo gg" onClick={this.unggPost}>
        <FavoriteIcon color="primary" />
      </MyButton>
    ) : (
      <MyButton tip="gg" onClick={this.ggPost}>
        <FavoriteBorder color="primary" />
      </MyButton>
    );
    return ggButton;
  }
}

GgButton.propTypes = {
  user: PropTypes.object.isRequired,
  postId: PropTypes.string.isRequired,
  ggPost: PropTypes.func.isRequired,
  unggPost: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user
});

const mapActionsToProps = {
  ggPost,
  unggPost
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(GgButton);