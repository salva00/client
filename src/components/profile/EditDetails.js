import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import MyButton from '../../utils/MyButton'
//REDUX
import  {connect} from 'react-redux';
import { editUserDetails } from '../../redux/actions/userAction';

//MUI
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import EditIcon from '@material-ui/icons/Edit';

const styles = (theme) =>({

})

class EditDetails extends Component {
    state = {
        bio: '',
        website: '',
        location:'',
        open:false
    };
    mapUserDetailsToState = (credentials) =>{
        this.setState({
            bio: credentials.bio ? credentials.bio : '',
            website: credentials.website ? credentials.website : '',
            location: credentials.location ? credentials.location : ''
        })
    };

    handleOpen=()=>{
        this.setState({open:true});
        this.mapUserDetailsToState(this.props.credentials);
    }
    handleClose = () =>{
        this.setState({open:false});
    }
    componentDidMount(){
       const { credentials } = this.props;
       this.mapUserDetailsToState(credentials);
       
    }

    handleChange= (event) =>{
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    handleSubmit=()=>{
        const userDetails = {
            bio: this.state.bio,
            website: this.state.website,
            location: this.state.location,
        };
        this.props.editUserDetails(userDetails);
        this.handleClose();
    }

    render() {
        const { classes } = this.props;
        return (
            <Fragment>
                {/* <Tooltip title="Edit details" placement="top">
                    <IconButton onClick={this.handleOpen} className={classes.buttom} >
                        <EditIcon color="primary"/>
                    </IconButton>
                </Tooltip> */}
                <MyButton tip="Modifica i tuoi dati" onClick={this.handleOpen} btnClassName={classes.buttom}>
                    <EditIcon color="primary"/>
                </MyButton>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    fullWidth
                    maxWidth="sm">
                        <DialogTitle>
                            Modifica i Tuoi dati
                        </DialogTitle>
                        <DialogContent>
                            <form>
                                <TextField
                                    name="bio"
                                    type="text"
                                    label="Bio"
                                    multiline
                                    rows="3"
                                    placeholder="Una breve biografia su di te..."
                                    className={classes.TextField}
                                    value={this.state.bio}
                                    onChange={this.handleChange}
                                    fullWidth
                                />
                                <TextField
                                    name="website"
                                    type="text"
                                    label="Website"
                                    placeholder="Il tuo sito"
                                    className={classes.TextField}
                                    value={this.state.website}
                                    onChange={this.handleChange}
                                    fullWidth
                                />
                                <TextField
                                    name="location"
                                    type="text"
                                    label="Città"
                                    placeholder="Inserisci la tua città!"
                                    className={classes.TextField}
                                    value={this.state.location}
                                    onChange={this.handleChange}
                                    fullWidth
                                />
                            </form>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="gray">
                                Annulla
                            </Button>
                            <Button onClick={this.handleSubmit} color="primary">
                                Salva
                            </Button>
                        </DialogActions>
                </Dialog>
            </Fragment>
        )
    }
}

EditDetails.propTypes = {
    editUserDetails: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired
}

const mapStateToProps = (state) =>({
    credentials: state.user.credentials
})

export default connect(mapStateToProps, {editUserDetails})(withStyles(styles)(EditDetails));
