import React, { Component, Fragment } from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import MyButton from '../../utils/MyButton'
import NewPost from '../post/NewPost'
//MUI
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
//icons
import HomeIcon from '@material-ui/icons/Home'
import ChatIcon from '@material-ui/icons/Chat';
import Notifications from './Notifications'

 class Navbar extends Component {
    
    render() {
        const { authenticated } = this.props;
        return (
            <AppBar position="fixed">
                <Toolbar className="nav-container">
                    {authenticated ? (
                        <Fragment>
                            <NewPost/>
                            <Link to="/">
                                <MyButton tip="Home">
                                    <HomeIcon color = "disabled"/>
                                </MyButton>
                            </Link>
                            <Link to="/chat">
                                <MyButton tip="Chats">
                                    <ChatIcon color = "disabled"/>
                                </MyButton>
                            </Link>
                            <Notifications />
                        </Fragment>


                    ):(
                        <Fragment>
                            <Button color="inherit" component={Link} to="/login">Login</Button>
                            <Button color="inherit" component={Link} to="/signup">Singup</Button>
                            <Button color="inherit" component={Link} to="/">Home</Button>
                            {/* <Button color="inherit" component={Link} to="/user">Profile</Button>*/}
                        </Fragment>
                    )}
                </Toolbar>
            </AppBar>
        )
    }
}

Navbar.propTypes = {
    authenticated:  PropTypes.bool.isRequired
}

const mapStateToProps = state =>({
    authenticated: state.user.authenticated
})

export default connect(mapStateToProps)(Navbar)
